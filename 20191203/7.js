window.addEventListener("load",function(e){
  let botonesNumericos=document.querySelectorAll(".numeros");
  let botonesColores=document.querySelectorAll(".colores");
  let salida=document.querySelector("#isalida");


  botonesNumericos.forEach(function(valor){
    valor.addEventListener("click", function(e) {
      salida.innerHTML+=e.target.innerHTML;
    });
  })

 
  botonesColores.forEach(function(valor){
    valor.addEventListener("click", function(e){
      salida.style.backgroundColor=e.target.getAttribute("data-color");
    });
  })

});